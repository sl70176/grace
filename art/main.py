#!/usr/bin/env python
"""
Create a Google Sheet from Race Roster (individual) results.

Notes for running locally:
 - you should be in the raceroster directory when you execute main.py
 - vars.json must be populated with the input variables
"""
import io
import json
import os

import art_parse
import google.auth

# custom modules
import google_sheets
import gspread
from google.oauth2 import service_account
from googleapiclient.discovery import build
from googleapiclient.http import MediaIoBaseDownload


def main(request):
    output = ""
    if request != "local":
        output += "<pre>"
    art_folder_id, art_distance, google_sheets_url = get_vars(request)
    output += f"Art Folder ID: {art_folder_id}"
    output += f"\nArt Distance (Meters): {art_distance}"
    output += f"\nGoogle Sheets URL: <a href='{google_sheets_url}' target='_blank'>{google_sheets_url}</a>"
    if request != "local":
        credentials, project_id = google.auth.default(
            scopes=[
                "https://spreadsheets.google.com/feeds",
                "https://www.googleapis.com/auth/drive",
            ]
        )
        gc = gspread.authorize(credentials)
    else:
        gc = gspread.service_account("../google_service_account.json")
    status, sh = google_sheets.get_sheet(gc, google_sheets_url)
    if status != 0:
        output += "\n\nERROR: Something went wrong accessing the Google Sheet\n"
        output += status
        print(output)
        return output
    output = download_file(request, art_folder_id, output)
    results, logs = art_parse.main(art_distance)
    if logs:
        output += logs
    if results:
        google_sheets.write(sh, results)
    output += f"\nProcessed {len(results)} results from ART!"
    if len(results) > 0:
        output += "\nWinner: {} in {}".format(results[0].athlete, results[0].guntime)
    print(output)
    if request != "local":
        output += "</pre>"
    # os.remove("/tmp/art.db")
    return output


def download_file(request, folder_id, output):
    if request != "local":
        credentials, project_id = google.auth.default(
            scopes=[
                "https://spreadsheets.google.com/feeds",
                "https://www.googleapis.com/auth/drive",
            ]
        )
    else:
        with open("../google_service_account.json", "r") as f:
            credz = json.load(f)
        credentials = service_account.Credentials.from_service_account_info(credz)
    drive_service = build("drive", "v3", credentials=credentials)
    page_token = None
    response = (
        drive_service.files()
        .list(
            q="'{}' in parents".format(folder_id),
            spaces="drive",
            fields="nextPageToken, " "files(id, name)",
            pageToken=page_token,
        )
        .execute()
    )
    files = response.get("files", [])
    files = [x for x in files if x.get("name").endswith(".csv")]
    if len(files) != 1:
        output += "\n\nERROR: Folder https://drive.google.com/drive/folders/{} contains more than one CSV file!\n".format(
            folder_id
        )
        print(output)
        exit(1)
    file_id = files[0].get("id")
    request = drive_service.files().get_media(fileId=file_id)
    fh = io.FileIO("/tmp/art.csv", "wb")  # this can be used to write to disk
    downloader = MediaIoBaseDownload(fh, request)
    done = False
    while done is False:
        status, done = downloader.next_chunk()
        print("Download %d%%." % int(status.progress() * 100))
    return output


def get_vars(request):
    """Get the input variables"""
    if request == "local":
        with open("./vars.json") as f:
            vars_json = json.load(f)
        art_folder_id = vars_json["art_folder_id"]
        art_distance = vars_json["art_distance"]
        google_sheets_url = vars_json["google_sheets_url"]
    else:  # it's in a cloud function
        try:
            request_json = request.get_json()
        except Exception:
            pass
        # get art_folder_id
        if request.args and "art_folder_id" in request.args:
            art_folder_id = request.args.get("art_folder_id")
        elif request_json and "art_folder_id" in request_json:
            art_folder_id = request_json["art_folder_id"]
        else:
            art_folder_id = f"No art_folder_id!"
        # get art_distance
        if request.args and "art_distance" in request.args:
            art_distance = request.args.get("art_distance")
        elif request_json and "art_distance" in request_json:
            art_distance = request_json["art_distance"]
        else:
            art_distance = f"No art_distance!"
        # get google_sheet_url
        if request.args and "google_sheets_url" in request.args:
            google_sheets_url = request.args.get("google_sheets_url")
        elif request_json and "google_sheets_url" in request_json:
            google_sheets_url = request_json["google_sheets_url"]
        else:
            google_sheets_url = f"No google_sheets_url!"
    return art_folder_id, art_distance, google_sheets_url


if __name__ == "__main__":
    main("local")
