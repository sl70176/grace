import csv
from datetime import datetime, timedelta
from pprint import pprint


def main(art_distance):
    logs = ""
    results_dict = {}
    results = []
    with open('/tmp/art.csv', newline='') as csvfile:
        rows = list(csv.reader(csvfile, delimiter=',', quotechar='"'))
    headings = rows[0]
    for i, row in enumerate(rows[1:]):
        distance = row[headings.index("Race Distance (in meters)")].strip()
        if distance != art_distance: # skip other events
            continue
        bib = row[headings.index("Bib #")]
        try:
            place = int(row[headings.index("Overall Place")].strip())
        except Exception:
            logs += f"\nWARNING: Unable to find place in row {i+2}, skipping"
            continue
        guntime = row[headings.index("Finishing Time")].strip()
        if not guntime:
            logs += f"\nWARNING: No Finishing Time in row {i+2}, skipping"
            continue
        if bib not in results_dict:
            results_dict[bib] = Result(place)
            results_dict[bib].bib = bib
            first_name = row[headings.index("First Name")].strip()
            last_name = row[headings.index("Last Name")].strip()
            results_dict[bib].athlete = f"{first_name} {last_name}"
            results_dict[bib].guntime = guntime
            results_dict[bib].gender = row[headings.index("Sex")].strip()
            categories = row[headings.index("Eligible Division(s)")].split("&")
            results_dict[bib].age = row[headings.index("Age")].strip()
            results_dict[bib].category = categories[-1].strip()
            results_dict[bib].chiptime = row[headings.index("Chip Time")].strip()
            results_dict[bib].city = row[headings.index("City")].strip()
            results_dict[bib].laps = 1
            results_dict[bib].cumulative_split_time = timedelta(seconds=0)
        else:
            results_dict[bib].laps += 1
        if "Split Times" in headings:
            split_heading = "Split Times"
        elif "Split Times (by chip time)" in headings:
            split_heading = "Split Times (by chip time)"
        else:
            logs += "ERROR: Can't find valid Split Times column"
            exit(1)
        if row[headings.index(split_heading)]:
            raw_split_time = row[headings.index(split_heading)].split()[-1].strip()
            split_time = str2dt(raw_split_time)
            if split_time:
                split_time = split_time - results_dict[bib].cumulative_split_time
                setattr(results_dict[bib], f"split{results_dict[bib].laps}", split_time)
                results_dict[bib].cumulative_split_time += split_time
            else:
                logs += f"\nWARNING: Unable to process split in row {i+2}, skipping"

    for k, v in results_dict.items():
        delattr(v, "cumulative_split_time")
        delattr(v, "laps")
        if not hasattr(v, "split2"):
            if hasattr(v, "split1"):
                delattr(v, "split1")
        results.append(v)
    results.sort(key=lambda x: x.place)

    return results, logs

def str2dt(rawtime):
    try:
        if "." in rawtime:
            dt = datetime.strptime(
                rawtime, "%H:%M:%S.%f"
            )
        else:
            dt = datetime.strptime(
                rawtime, "%H:%M:%S"
            )
        delta = timedelta(hours=dt.hour, minutes=dt.minute, seconds=dt.second, microseconds=dt.microsecond)
    except Exception:
        delta = False
    return delta

class Result:
    def __init__(self, place):
        self.place = place

    def __repr__(self):
        return "Result(place={}, athlete={}, guntime={})".format(
            self.place,
            self.athlete,
            self.guntime,
        )
