#!/bin/bash
gcloud functions deploy art \
 --runtime python39 \
 --region northamerica-northeast1 \
 --allow-unauthenticated \
 --entry-point main \
 --set-env-vars art_folder_id=ART_FOLDER_ID,art_distance=ART_DISTANCE,google_sheets_url=GOOGLE_SHEETS_URL \
 --trigger-http
