#!/bin/bash
python3 -m venv /usr/local/venv/grace
source /usr/local/venv/grace/bin/activate
python -m pip install -r requirements.txt
python -m pip install -r art/requirements.txt
